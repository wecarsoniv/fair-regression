# ----------------------------------------------------------------------------------------------------------------------
# FILE DESCRIPTION
# ----------------------------------------------------------------------------------------------------------------------

# File : fair_regression.py
# Author : Billy Carson
# Date written : 02-06-2020
# Last modified : 03-26-2021

"""
Description :  Python implementation of the fair regression model proposed in "A Convex Framework for Fair Regression"
by Berk et al. (2017).
"""


# ----------------------------------------------------------------------------------------------------------------------
# IMPORT MODULES
# ----------------------------------------------------------------------------------------------------------------------

# Import modules
import numpy as np
from numpy import mean, exp, log, zeros, around, clip, unique, where, intersect1d, array_equal
from numpy.linalg import norm
from scipy.optimize import minimize


# ----------------------------------------------------------------------------------------------------------------------
# CLASS DEFINITIONS
# ----------------------------------------------------------------------------------------------------------------------

# Not fitted error class
class NotFittedError(ValueError, AttributeError):
    """
    Description
    -----------
    NotFittedError class. Raises exception if methods are called that require an unfitted model to first be fit to data.
    
    Parameters
    ----------
    ValueError : exception; raised when an argument has the right type but an inappropriate value
    AttributeError : exception; raised when an attribute reference or assignment fails
    
    Attributes
    ----------
    N/A
    
    Methods
    -------
    N/A
    """
    pass


# Fair logistic regression
class FairLogisticRegression():
    """
    Description
    -----------
    Object definition for the fair regressor proposed in "A Convex Framework for Fair Regression" by Berk et al.
    (2017).
    
    Parameters
    ----------
    
    Attributes
    ----------
    w : np.ndarray; model weights; initialized to None
    penalty : str; penalty type; default = 'l2'
    gamma : float; normalization penalty magnitude; default = 1.0
    fair_penalty : str; type of fair penalty; default = 'individual'
    lam : float; fairness constraint magnitude; default = 1.0
    solver : str; minimization method; default = 'BFGS'
    n_1 : int; number of samples from sensitive class 1; initialized to None
    n_2 : int; number of samples from sensitive class 2; initialized to None
    eps : float; small value used to stabilize process of making predictions; default = 1e-15
    is_fitted : bool; indicates whether model has been fitted; initialized to False
    
    Methods
    -------
    __init__() : Model instantiation method.
    fit() : Fits the model to the data.
    predict() : Generates fair class predictions.
    predict_proba() : Generates fair class prediction probabilities.
    get_z_vals() : Generates and returns z-values (model predictions before being passed through sigmoid non-linearity).
    _calc_loss() : Calculates model loss given the data.
    """
    
    # Instantiation method
    def __init__(self, penalty='l2', gamma=1.0, fair_penalty='individual', lam=1.0, solver='BFGS'):
        """
        Description
        -----------
        Instantiation method for FairLogisticRegression object.
        
        Parameters
        ----------
        penalty : str; penalty type; default = 'l2'
        gamma : float; normalization penalty magnitude; default = 1.0
        fair_penalty : str; type of fair penalty; default = 'individual'
        lam : float; fairness constraint magnitude; default = 1.0
        solver : str; minimization method; default = 'BFGS'
        
        Returns
        -------
        N/A
        """
        
        # Assign fair logistic regression object attributes
        self.w = None
        self.penalty = penalty
        self.gamma = gamma
        self.fair_penalty = fair_penalty
        self.lam = lam
        self.solver = solver
        self.n_1 = None
        self.n_2 = None
        self.eps = 1e-15
        self.is_fitted = False

    # Fit fair logisitic regression classifier
    def fit(self, X, y_labels, y_sens):
        """
        Description
        -----------
        Model fit method.
        
        Parameters
        ----------
        X : np.ndarray; (n x p) array of features
        y_labels : np.ndarray; (n x 1) array of labels
        y_sens : np.ndarray; (n x 1) array of sensitive class memberships
        
        Returns
        -------
        self : reference to instantiated model
        """
        
        # Minimize fair logistic regression loss function
        res = minimize(fun=self._calc_loss, x0=zeros(shape=X.shape[1]), args=(X, y_labels, y_sens), method=self.solver)

        # Retrieve weights and update fitted variable
        self.w = res.x
        self.is_fitted = True

        # Return self
        return self

    # Generate fair predictions
    def predict(self, X):
        """
        Description
        -----------
        Model prediction method.
        
        Parameters
        ----------
        X : np.ndarray; (n x p) array of features
        
        Returns
        -------
        y_pred : np.ndarray; (n x 1) array of fair predictions
        """
        
        # Raise NotFittedError if model is not fitted
        if not self.is_fitted:
            error_msg = ("This %(name)s instance is not fitted yet. Call 'fit' with "
                         "appropriate arguments before using this method.")
            raise NotFittedError(error_msg % {'name': self.__class__.__name__})
        
        # Make class predictions from features X
        y_pred = around(1.0 / (1.0 + exp(-(X @ self.w))))
        
        # Return predictions
        return y_pred

    # Generate fair prediction probabilities
    def predict_proba(self, X):
        """
        Description
        -----------
        Model prediction probabilities method.
        
        Parameters
        ----------
        X : np.ndarray; (n x p) array of features
        
        Returns
        -------
        y_prob : np.ndarray; (n x 1) array of fair prediction probabilities
        """
        
        # Raise NotFittedError if model is not fitted
        if not self.is_fitted:
            error_msg = ("This %(name)s instance is not fitted yet. Call 'fit' with "
                         "appropriate arguments before using this method.")
            raise NotFittedError(error_msg % {'name': self.__class__.__name__})
        y_prob = 1.0 / (1.0 + exp(-(X @ self.w)))
        
        # Return prediction probabilities
        return y_prob

    # z value retrieval
    def get_z_vals(self, X):
        """
        Description
        -----------
        Returns generated model z-values.
        
        Parameters
        ----------
        X : np.ndarray; (n x p) array of features
        
        Returns
        -------
        z_vals : np.ndarray; (n x 1) array of z-values (model predictions before non-linear transformation)
        """
        
        # Raise NotFittedError if model is not fitted
        if not self.is_fitted:
            error_msg = ("This %(name)s instance is not fitted yet. Call 'fit' with "
                         "appropriate arguments before using this method.")
            raise NotFittedError(error_msg % {'name': self.__class__.__name__})
        z_vals = X @ self.w
        
        # Return z values
        return z_vals

    # Calculate loss
    def _calc_loss(self, w, X, y_labels, y_sens):
        """
        Description
        -----------
        Calculate model loss given the data.
        
        Parameters
        ----------
        w : np.ndarray; (p x 1) array of model weights
        X : np.ndarray; (n x p) array of features
        y_labels : np.ndarray; (n x 1) array of labels
        y_sens : np.ndarray; (n x 1) array of sensitive class memberships
        
        Returns
        -------
        loss : float; model loss
        """
        
        # Check sensitive labels only contain values 1 and 2
        if not array_equal(unique(y_sens), np.array([1, 2])):
            error_msg = ('%(name) only supports sensitive group labels of 1 and 2. '
                         'Redefine sensitive labels array to only include numbers 1 and 2.')
            raise ValueError(error_msg % {'name': self.__class__.__name__})

        # Number of samples in each group
        n_1 = len(where(y_sens == 1))
        n_2 = len(where(y_sens == 2))

        # Split into groups based on class label and sensitive feature label
        X_01 = X[intersect1d(np.where(y_labels == 0), where(y_sens == 1)), :]
        X_02 = X[intersect1d(np.where(y_labels == 0), where(y_sens == 2)), :]
        X_11 = X[intersect1d(np.where(y_labels == 1), where(y_sens == 1)), :]
        X_12 = X[intersect1d(np.where(y_labels == 1), where(y_sens == 2)), :]

        # Individual penalty
        if self.fair_penalty == 'individual':
            fair_loss = 0
            for i in range(X_01.shape[0]):
                for j in range(X_02.shape[0]):
                    fair_loss += ((X_01[i, :] @ w) - (X_02[j, :] @ w)) ** 2
            for i in range(X_11.shape[0]):
                for j in range(X_12.shape[0]):
                    fair_loss += ((X_11[i, :] @ w) - (X_12[j, :] @ w)) ** 2
            fair_loss = fair_loss / (n_1 * n_2)

        # Group penalty
        elif self.fair_penalty == 'group':
            fair_loss = 0
            for i in range(X_01.shape[0]):
                for j in range(X_02.shape[0]):
                    fair_loss += (X_01[i, :] @ w) - (X_02[j, :] @ w)
            for i in range(X_11.shape[0]):
                for j in range(X_12.shape[0]):
                    fair_loss += (X_11[i, :] @ w) - (X_12[j, :] @ w)
            fair_loss = (fair_loss / (n_1 * n_2)) ** 2

        # No fair penalty
        elif self.fair_penalty is None:
            fair_loss = 0

        # Default fair penalty (default to individual)
        else:
            fair_loss = 0
            for i in range(X_01.shape[0]):
                for j in range(X_02.shape[0]):
                    fair_loss += ((X_01[i, :] @ w) - (X_02[j, :] @ w)) ** 2
            for i in range(X_11.shape[0]):
                for j in range(X_12.shape[0]):
                    fair_loss += ((X_11[i, :] @ w) - (X_12[j, :] @ w)) ** 2
            fair_loss = fair_loss / (n_1 * n_2)

        # Calculate predictions
        pred = 1.0 / (1.0 + exp(-(X @ w)))
        pred_clip = clip(pred, self.eps, 1 - self.eps)

        # Calculate log loss between labels and predictions
        log_loss = mean(-((y_labels * log(pred_clip)) + ((1.0 - y_labels) * log(1.0 - pred_clip))))

        # Combine losses
        loss = log_loss + (self.lam * fair_loss) + (self.gamma * norm(w))

        # Return loss value
        return loss

