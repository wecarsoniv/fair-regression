# Fair Regression Implementation


## Introduction

This project is a Python implementation of the method proposed in the paper [*A Convex Framework for Fair Regression* by Berk et al. (2017)](https://arxiv.org/abs/1706.02409).


## Technologies

This project relies on the following technologies:

* Python >= 3.6
* [NumPy](https://numpy.org/) >= 1.19.2
* [SciPy](https://www.scipy.org/) >= 1.5.2


## Files

Implementation of the fair regressor proposed by Berk et al. (2017) can be found in `fair_regression.py`.

